#!/usr/bin/env bash

/usr/bin/mongod --fork --dbpath=/var/lib/mongodb --logpath /log/mongod.log
#--smallfiles --replSet replSetName --quiet

RESULT=$(ps -aux | grep mongod)

##check if MongoDB is running and if not start the process
if [ -z "${RESULT}" ]; then
	echo "MongoDB is not running"
else
   	mongoimport --jsonArray -d malware -c urls --file ./scripts/urls.json
fi

tail -f /log/mongod.log



