package main

import (
	"net/http"
)

type Route struct {
	Name        string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"UrlNoPath",
		"/urlinfo/1/{url}",
		ReadUrl,
	},
	Route{
		"UrlWithPath",
		"/urlinfo/1/{url}/{pathquery:.*$}",
		ReadUrl,
	},
	Route{
		"DeleteAddUrl",
		"/urlinfo/1",
		ManageUrl,
	},
}
