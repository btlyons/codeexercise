From golang:1.7

ADD . /go/src/lookup

RUN go get github.com/gorilla/mux && \
	go get gopkg.in/mgo.v2/bson && \
	go get gopkg.in/mgo.v2 && \
    go install lookup


EXPOSE 8080

ENTRYPOINT ["/go/bin/lookup"]
CMD ["-database=mongo"]