# URL lookup service
A small web service, in Go, that responds to GET requests where the caller passes in a URL and the service responds with some information about that URL. The GET requests look like this:

    GET /urlinfo/1/{hostname_and_port}/{original_path_and_query_string}
#
# Directories
* compose - docker-compose file
* docs - test coverage.html
    * Open in broswer to view coverage from unit test
* K8s - Resource files for deployment on a Kubernetes node
* mongo - database Dockerfile and scripts dir
    * scripts/insert.sh - entrypoint script for Dockerfil
    * scripts/urls.json - list of demo urls
#
# Docker-compose 
###### The docker-compose file setups a mongo replica set and the lookup service.
###### If using this method, you need to add URLs to the list by either using the endpoints documented below or exec into the Primary node (mongo_1_1 container) and add them.  
###### If you exec in the primary node, you can run the following command from the scripts dir:

#
    mongoimport --jsonArray -d malware -c urls --file ./scripts/urls.json
###### From the compose dir run the following command:
#
    docker-compose up -d
    
# Lookup Service
#### Build the standalone container.
###### From the root dir run the following command:
#
      docker build -t lookup .
      
#### Run the lookup container
      docker run -p 8080:8080 --link <containerName>:<conatinerName> lookup -database=<containerName>

#### Complete test
    docker run -p 8080:8080 --link mongo:mongo lookup -database=mongo:27017

# MongoDB
#### The single mongo instance has prepopulated data in the collection.
#### From the mongo dir run the following command:
    docker build -t mongo .
    run --name=mongo -p 27017:27017 mongo
#### Option 2:
    docker pull btlyons1/mongo
    run --name=mongo -p 27017:27017 mongo
#
# API Documentation

##### GET /urlinfo/1/{hostname_and_port}/{original_path_and_query_string}
   * Returns JSON Status code
     * {"Status": "200 Ok"} If the URL is not in the list
     * {"Status": "403 Forbidden"} If the URL is in the list
     * {"Status": "405 Method Not Allowed"} If the method is not GET, OPTIONS

##### POST /urlinfo/1
   * Data in body {"url": "example.com"}
   * Returns JSON Status code
     * {"Created" [ "url": "example.com, "_id": "objetIdStaus" ] "Status": "201 Created"} If the URL was successfully created
     * {"Status": "409 Conflict"} If the URL is already in the list
     * {"Status": "405 Method Not Allowed"} If the method is not POST, DELETE, OPTIONS

##### DELETE /urlinfo/1
   * Data in body {"url": "example.com"}
   * Returns JSON Status code
     * {"Status": "200 Ok"} If the URL was successfully deleted
     * {"Status": "404 Not Found"} If the data was successfully processed but the URL does not exist
     * {"Status": "405 Method Not Allowed"} If the method is not POST, DELETE, OPTIONS

### Future iteration would have the following:
 * Input sanitization
 * Concurrent batch queries and inserts
 * Benchmarking
 * Mongo interface for testing
 * TBD

# 
# Give some thought to the following:
###### The size of the URL list could grow infinitely, how might you scale this beyond the memory capacity of this VM? Bonus if you implement this.
  * Utilize clustered dbs. Mongo has built-in sharding that supports distribution of data across multiple nodes. You can use mongos (MongoDB Shard) to obtain horizontal scaling.
    * The mgo driver for Golang will let you specify multiple seed servers to connect to.
    * Aggregation is done in parallel across nodes
    * Mongo sharding intelligently routes to the correct node
  * If the list is extremely large, you can implement a file storage.
    * Mongo drivers support the use of GridFS to store large files. Kind of like a file system.
      * Built on top of Mongo means that it can be fully replicated and distributed.

###### The number of requests may exceed the capacity of this VM, how might you solve that? Bonus if you implement this.
  * Distribute the load by geolocating the VMs in applicable availability zones
    * Use a "global" load balancer at each availability zone
    * Use Amazon CLoudFront Content Delivery Network
  * Configure an orchestrator which implements a horizontal autoscaler to scale based on CPU utilization e.g., Kubernetes
  * Use a language that supports concurrency

###### What are some strategies you might use to update the service with new URLs? Updates may be as much as 5 thousand URLs a day with updates arriving every 10 minutes.
  * Handling updates concurrently with Golang
    * Utilizing the session.Copy method which enables each copy to use a different connection
    * Once session copy is closed, the socket returns to the pool
    * Allow the ability to do URL batch updates with concurrent loading.
      * For the session, you can use the SetBatch(500) method to load an arbitrary number or urls.
      * Then you can set SetPrefetch(.25) to load the next batch when 75% of the previous batch has completed or whatever float value between 0 - 1.
      * You use the iter method to iterate over the urls

