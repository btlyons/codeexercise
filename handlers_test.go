package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

type TestHttp struct {
	url string `json:"url"`
}

func TestReadUrl(t *testing.T) {
	router := NewRouter()
	router.HandleFunc("/urlinfo/1/{url}", func(w http.ResponseWriter, r *http.Request) {

	})
	ts := httptest.NewServer(router)
	defer ts.Close()
	urls := []string{"google.com", "BeerIsGood.com", "facebook.com"}
	for _, url := range urls {
		url := ts.URL + "/urlinfo/1/" + url
		_, err := http.Get(url)
		if err != nil {
			t.Fatal(err)
		}
	}
}

func TestCreateUrl(t *testing.T) {
	url, _ := json.Marshal(TestHttp{url: "btlyons1.com"})
	r, _ := http.NewRequest("POST", "/urlinfo/1", bytes.NewBuffer(url))
	w := httptest.NewRecorder()
	NewRouter().ServeHTTP(w, r)

	url2, _ := json.Marshal(TestHttp{url: "btlyons1.com"})
	r2, _ := http.NewRequest("POST", "/urlinfo/1", bytes.NewBuffer(url2))
	w2 := httptest.NewRecorder()
	NewRouter().ServeHTTP(w2, r2)
}

func TestRemoveUrl(t *testing.T) {
	url, _ := json.Marshal(TestHttp{"google.com"})
	r, _ := http.NewRequest("DELETE", "/urlinfo/1", bytes.NewBuffer(url))
	w := httptest.NewRecorder()
	NewRouter().ServeHTTP(w, r)

	url2, _ := json.Marshal(TestHttp{"google.com"})
	r2, _ := http.NewRequest("DELETE", "/urlinfo/1", bytes.NewBuffer(url2))
	w2 := httptest.NewRecorder()
	NewRouter().ServeHTTP(w2, r2)
}
