package main

import (
	"testing"
	// "gopkg.in/mgo.v2"
)

func Test_GetUrl(t *testing.T) {
	dbConnection = NewMongo()
	type args struct {
		url string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Successful - find malware url",
			args: args{
				url: "google.com",
			},
			wantErr: false,
		},
		{
			name: "Unsuccessful - url not in db ",
			args: args{
				url: "beerIsGood.com",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := dbConnection.GetUrl(tt.args.url); (err != nil) != tt.wantErr {
				t.Errorf("GetUrl() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_PostUrl(t *testing.T) {
	dbConnection = NewMongo()
	type args struct {
		url string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Unsuccessful - inserting url that already exists",
			args: args{
				url: "google.com",
			},
			wantErr: true,
		},
		{
			name: "Successful - inserting new url",
			args: args{
				url: "lindaLyons.com",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := dbConnection.PostUrl(tt.args.url); (err != nil) != tt.wantErr {
				t.Errorf("PostUrl() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_DeleteUrl(t *testing.T) {
	dbConnection = NewMongo()
	type args struct {
		url string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "Successful - delete url in db",
			args: args{
				url: "lindaLyons.com",
			},
			wantErr: false,
		},
		{
			name: "Unsuccessful - delete url that is not in db ",
			args: args{
				url: "btlyons1.com",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := dbConnection.DeleteUrl(tt.args.url); (err != nil) != tt.wantErr {
				t.Errorf("DeleteUrl() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
