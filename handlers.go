package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"strings"
)

type JsonResponse map[string]interface{}

func ReadUrl(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		vars := mux.Vars(r)
		url := strings.ToLower(vars["url"])
		err := dbConnection.GetUrl(url)
		if err != nil {
			responseMessage(w, JsonResponse{"Status": "200 OK"}, 200)
		} else {
			responseMessage(w, JsonResponse{"Status": "403 Forbidden"}, 403)
		}
	} else if r.Method == "OPTIONS" {
		w.Header().Set("Allow", "GET, OPTIONS")
		w.WriteHeader(http.StatusOK)

	} else {
		responseMessage(w, JsonResponse{"Status": "405 Method Not Allowed"}, 405)
	}
}

func ManageUrl(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		reqBodyStruct := new(MalwareDoc)
		if err := json.NewDecoder(r.Body).Decode(&reqBodyStruct); err != nil {
			responseMessage(w, JsonResponse{"Status": "400 Bad Request"}, 400)
		} else {
			if newUrl, err := dbConnection.PostUrl(strings.ToLower(reqBodyStruct.Url)); err != nil {
				responseMessage(w, JsonResponse{"Status": "409 Conflict"}, 409)
			} else {
				responseMessage(w, JsonResponse{"Status": "201 Created", "Created": newUrl}, 201)
			}
		}
	} else if r.Method == "OPTIONS" {
		w.Header().Set("Allow", "POST, DELETE, OPTIONS")
		w.WriteHeader(http.StatusOK)
	} else if r.Method == "DELETE" {
		reqBodyStruct := new(MalwareDoc)
		if err := json.NewDecoder(r.Body).Decode(&reqBodyStruct); err != nil {
			responseMessage(w, JsonResponse{"Status": "400 Bad Request"}, 400)
		}
		if err := dbConnection.DeleteUrl(strings.ToLower(reqBodyStruct.Url)); err != nil {
			responseMessage(w, JsonResponse{"Status": "404 Not Found"}, 404)

		} else {
			responseMessage(w, JsonResponse{"Status": "200 OK"}, 200)
		}

	} else {
		responseMessage(w, JsonResponse{"Status": "405 Method Not Allowed"}, 405)
	}
}

func responseMessage(w http.ResponseWriter, data interface{}, status int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	response, err := ToJSON(data)
	if err != nil {
		w.Header().Set("Content-Type", "text/plain")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(response)

}

func ToJSON(data interface{}) ([]byte, error) {
	b, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	return b, nil
}
