package main

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
)

type MalwareDoc struct {
	ID  bson.ObjectId `bson:"_id,omitempty"`
	Url string        `bson:"url"`
}

type mongoDatastore struct {
	db *mgo.Session
}

//Constructor
func NewMongo() *mongoDatastore {
	// If timeout is zero, the call may block forever waiting for a connection to be made.
	db, err := mgo.DialWithTimeout("mongodb://"+dbUri, 0)
	if err != nil {
		log.Fatal("Cannot dial mongo ", err)
	} else {
		log.Println("Connected to ", dbUri)
	}
	db.SetSafe(&mgo.Safe{})
	db.SetMode(mgo.Monotonic, true)

	return &mongoDatastore{
		db,
	}

}

func (mongoDatastore *mongoDatastore) GetUrl(url string) (err error) {
	result := MalwareDoc{}
	sessionCopy := mongoDatastore.db.Copy()
	defer sessionCopy.Close()
	collection := sessionCopy.DB("malware").C("urls")
	err = collection.Find(bson.M{"url": url}).One(&result)
	if err != nil {
		return
	}
	return nil
}

func (mongoDatastore *mongoDatastore) PostUrl(url string) (newUrl MalwareDoc, err error) {
	sessionCopy := mongoDatastore.db.Copy()
	defer sessionCopy.Close()
	collection := sessionCopy.DB("malware").C("urls")
	newUrl = MalwareDoc{ID: bson.NewObjectId(), Url: url}
	_, err = collection.Upsert(bson.M{"url": url}, newUrl)
	if err != nil {
		return
	}
	return
}

func (mongoDatastore *mongoDatastore) DeleteUrl(url string) (err error) {
	sessionCopy := mongoDatastore.db.Copy()
	defer sessionCopy.Close()
	sessionCopy.SetSafe(&mgo.Safe{})
	collection := sessionCopy.DB("malware").C("urls")
	err = collection.Remove(bson.M{"url": url})
	if err != nil {
		return
	}
	return nil
}
