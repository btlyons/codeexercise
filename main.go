package main

import (
	"flag"
	"log"
	"net/http"
)

var dbConnection *mongoDatastore
var dbUri string

func init() {
	flag.StringVar(&dbUri, "database", "", " ")
	flag.Parse()
}

func main() {
	dbConnection = NewMongo()
	router := NewRouter()

	log.Fatal(http.ListenAndServe(":8080", router))
}
